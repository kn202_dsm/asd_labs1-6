﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASD_Lab7_8
{
    public static class Graph
    {
        public const ushort max = 19;
        public static int[,] adjacency = new int[max, max];
        static bool[] visited = new bool[max];

        static string[] cities = {
            "Киев",
            "Житомир",
            "Новоград-Волынский",
            "Ровно",
            "Луцк",
            "Бердичев",
            "Винница",
            "Хмельницкий",
            "Тернополь",
            "Шепетовка",
            "Белая церковь",
            "Черкасы",
            "Кременчук",
            "Умань",
            "Полтава",
            "Харковь",
            "Прилуки",
            "Сумы",
            "Миргород"
        };
        public static int 
            allDistance = 0, 
            distance = 0;

        public static void InitAdjacency()
        {
            for (int i = 0; i < adjacency.GetLength(0); i++)
                for (int j = 0; j < adjacency.GetLength(1); j++)
                    adjacency[i, j] = 0;
            adjacency[0, 1] = 135;
            adjacency[1, 4] = 80;
            adjacency[4, 7] = 100;
            adjacency[7, 8] = 68;
            adjacency[5, 9] = 73;
            adjacency[9, 10] = 110;
            adjacency[10, 11] = 104;
            adjacency[1, 6] = 115;
            adjacency[0, 2] = 78;
            adjacency[2, 12] = 115;
            adjacency[2, 13] = 146;
            adjacency[13, 15] = 105;
            adjacency[2, 14] = 181;
            adjacency[14, 16] = 130;
            adjacency[0, 3] = 128;
            adjacency[3, 17] = 175;
            adjacency[17, 18] = 109;
        }

        public static void PrintAdjacency()
        {
            for (int i = 0; i < adjacency.GetLength(0); i++)
            {
                for (int j = 0; j < adjacency.GetLength(1); j++)
                    Console.Write(adjacency[i, j]+"\t");
                Console.WriteLine();
            }
        }

        public static void NewDatas()
        {
            for (int i = 0; i < visited.Length; i++)
                visited[i] = false;
            distance = 0;
            allDistance = 0;
        }

        public static void DFS(int v)
        {
            Console.WriteLine($"{cities[v]}({allDistance}) -> ");
            visited[v] = true;

            for (int i = 0; i < max; i++)
            {
                if (adjacency[v, i] != 0 && !visited[i])
                {
                    distance += adjacency[v, i];
                    allDistance += adjacency[v, i];
                    DFS(i);
                }
            }
            allDistance += distance;
            distance = 0;
        }


        static int[] arr = new int[19];
        public static void BFS(int f)
        {
            bool[] visited = new bool[19];
            string start = cities[f];
            visited[f] = true;
            var queue = new Queue<int>();
            queue.Enqueue(f);
            while (queue.Count != 0)
            {
                f = queue.Dequeue();
                for (int i = 0; i < 19; i++)
                {
                    if (adjacency[f, i] != 0 && !visited[i])
                    {
                        visited[i] = true;
                        queue.Enqueue(i);
                        arr[i] += (arr[f] + adjacency[f, i]);
                        Console.WriteLine(start + " -> " + cities[i] + ": " + arr[i]);
                    }
                }
            }
        }
    }
}
