﻿using System;

namespace ASD_Lab7_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Graph.InitAdjacency();
            Graph.PrintAdjacency();
            Graph.NewDatas();
            Console.Write($"\nDFS \n");
            Graph.DFS(0);
            Console.Write($"Киев (всё расстояние): {Graph.allDistance}\n");

            Graph.NewDatas();
            Console.Write($"\nBFS \n");
            Graph.BFS(0);
        }
    }
}
