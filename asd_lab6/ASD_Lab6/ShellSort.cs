﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASD_Lab6
{
    public static class ShellSort
    {
        public static void Sort(int[] arr)
        {
            int step = arr.Length / 2;
            while (step > 0)
            {
                int i, j;
                for (i = step; i < arr.Length; i++)
                {
                    int value = arr[i];
                    for (j = i - step; 
                        (j >= 0) && (arr[j] > value); 
                        j -= step)
                        arr[j + step] = arr[j];
                    arr[j + step] = value;
                }
                step /= 2;
            }
        }

        public static int[] GetRandomArray(int arraySize)
        {
            Random random = new Random();
            int[] randomArray = new int[arraySize];
            for (int i = 0; i < randomArray.Length; i++)
            {
                randomArray[i] = random.Next(400);
            }

            return randomArray;
        }
    }
}
