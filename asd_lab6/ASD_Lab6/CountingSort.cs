﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASD_Lab6
{
    public static class CountingSort
    {
        public static short[] Sort(short[] array)
        {
            //поиск минимального и максимального значений
            var min = array[0];
            var max = array[0];
            foreach (short element in array)
            {
                if (element > max)
                {
                    max = element;
                }
                else if (element < min)
                {
                    min = element;
                }
            }

            //поправка
            var correctionFactor = min != 0 ? -min : 0;
            max += (short)correctionFactor;

            var count = new int[max + 1];
            for (var i = 0; i < array.Length; i++)
            {
                count[array[i] + correctionFactor]++;
            }

            var index = 0;
            for (var i = 0; i < count.Length; i++)
            {
                for (var j = 0; j < count[i]; j++)
                {
                    array[index] = (short)(i - correctionFactor);
                    index++;
                }
            }

            return array;
        }

        public static short[] GetRandomArray(int arraySize)
        {
            Random random = new Random();
            short[] randomArray = new short[arraySize];
            for (int i = 0; i < randomArray.Length; i++)
            {
                randomArray[i] = (short)random.Next(-10, 100);
            }

            return randomArray;
        }

    }
}
