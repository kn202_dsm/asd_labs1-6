﻿using ASD_Lab6;
using System;
using System.Diagnostics;

namespace ASD_Lab6
{
    class Program
    {

        static int CheckedValue()
        {
            int num;
            while(!int.TryParse(Console.ReadLine(), out num))
            {
                Console.WriteLine("Неверное значение, попробуйте ещё раз");
            }
            return num;
        }

        static void Main(string[] args)
        {
            Stopwatch t = new Stopwatch();
            TimeSpan time = new TimeSpan();
            Console.WriteLine("Введите способ сортировки:\n 1.Пирамидальная\n 2.Шелла\n 3.Подсчётом");
            //switch (CheckedValue())
            switch (3)
            {
                case 1: {
                        Console.Write("Введите длинну масива: ");
                        int n = CheckedValue();
                        for (int i = 0; i < 1000; i++)
                        {
                            float[] arr = HeapSort.GetRandomArray(n);
                            ///Console.WriteLine("Входные данные: \n{0}", string.Join("\t", arr));
                            t.Start();
                            HeapSort.Sort(arr);
                            t.Stop();
                            //Console.WriteLine("Отсортированый массив: \n{0}", string.Join("\t", arr));
                            time += t.Elapsed;
                        }
                        time /= 1000;
                        Console.WriteLine(time);
                    } break;
                case 2:{
                        Console.Write("Введите длинну масива: ");
                        int n = CheckedValue();
                        for (int i = 0; i < 1000; i++)
                        {
                            int[] arr = ShellSort.GetRandomArray(n);
                            //Console.WriteLine("Входные данные: \n{0}", string.Join("\t", arr));
                            t.Start();
                            ShellSort.Sort(arr);
                            t.Stop();
                            //Console.WriteLine("Отсортированый массив: \n{0}", string.Join("\t", arr));
                            time += t.Elapsed;
                        }
                        time /= 1000;
                        Console.WriteLine(time);
                    }
                    break;
                case 3: {
                        Console.Write("Введите длинну масива: ");
                        int n = CheckedValue();
                        for (int i = 0; i < 1000; i++)
                        {
                            short[] arr = CountingSort.GetRandomArray(n);
                            //Console.WriteLine("Входные данные: \n{0}", string.Join("\t", arr));
                            t.Start();
                            arr = CountingSort.Sort(arr);
                            t.Stop();
                            //Console.WriteLine("Отсортированый массив: \n{0}", string.Join("\t", arr));
                            time += t.Elapsed;
                        }
                        time /= 1000;
                        Console.WriteLine(time);
                    } break;
                default: break;
            }
        }
    }
}
