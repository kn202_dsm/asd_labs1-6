﻿// ASD_Lab2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#define ARRM 40000

typedef long long urand;

urand next = 1;

urand my_rand()
{
    urand m = pow(2, 32); // range > 0 to m random m > 0
    urand a = 22695477; // multiplier > 0 <= a <= m)
    urand inc = 1; // inc > random 0 <= inc <= m
    next = (a * next + inc) % m; // defined formula 
    return (next);
}

void my_srand(urand seed)
{
    next = seed;
}

void printarr(long* arr) {
	printf("array = {%d", arr[0]);
	for (int i = 1; i < ARRM; i++)
		printf(", %d", arr[i]);
	printf("}\n");
}

// math expection
double avg(long* arr) {
	urand sum = 0;
	for (int i = 0; i < ARRM; i++)
		sum += *(arr + i);
	return double(sum / ARRM);
}

// dispersion (average of square)
urand dispersion(long* arr)
{
	int size = ARRM;
	// unsigned long long devi;
	urand sum2 = 0;
	double average = avg(arr);
	for (int i = 0; i < size; i++)
		sum2 += pow((double(arr[i]) - average), 2);
	// devi = sqrt(sum2 / (size - 1));
	return sum2;
}

// deviation (STD)
double deviation(long* arr)
{
	int size = ARRM;
	urand d = dispersion(arr);
	return (double)sqrt(d / (size - 1));
}

int main() {
	my_srand(123546);
	long arr[ARRM];
	for (int i = 0; i < ARRM; i++)
		arr[i] = my_rand() % 250;
	printarr(arr);
	double average = avg(arr);
	urand disp = dispersion(arr);
	double devi = deviation(arr);
	printf("Average = %f\n", average);
	printf("Dispersion = %d\n", disp);
	printf("Deviation = %f\n", devi);
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
