﻿#include <iostream>

int main()
{
	union {
		float f;
		struct {
			unsigned int mantisa : 23;
			unsigned int exponent : 8;
			unsigned int sign : 1;
		} parts;
	} float_trace;

	union {
		float f;
		//  bits  //
		struct {
			unsigned int a32 : 1; unsigned int a31 : 1;
			unsigned int a30 : 1; unsigned int a29 : 1;
			unsigned int a28 : 1; unsigned int a27 : 1;
			unsigned int a26 : 1; unsigned int a25 : 1;
			unsigned int a24 : 1; unsigned int a23 : 1;
			unsigned int a22 : 1; unsigned int a21 : 1;
			unsigned int a20 : 1; unsigned int a19 : 1;
			unsigned int a18 : 1; unsigned int a17 : 1;
			unsigned int a16 : 1; unsigned int a15 : 1;
			unsigned int a14 : 1; unsigned int a13 : 1;
			unsigned int a12 : 1; unsigned int a11 : 1;
			unsigned int a10 : 1; unsigned int a9 : 1;
			unsigned int a8 : 1; unsigned int a7 : 1;
			unsigned int a6 : 1; unsigned int a5 : 1;
			unsigned int a4 : 1; unsigned int a3 : 1;
			unsigned int a2 : 1; unsigned int a1 : 1;
		} parts;
	} float_bits;

	float_trace.f;
	printf("Enter float value: ");
	scanf_s("%f", &float_trace.f);
	float_bits.f = float_trace.f;
	printf("sign = %X(hex)\n", float_trace.parts.sign);
	printf("exponent = %X(hex)\n", float_trace.parts.exponent);
	printf("mantisa = %X(hex)\n", float_trace.parts.mantisa);
	printf("%d %d%d%d%d%d%d%d%d %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n",
		float_bits.parts.a1, float_bits.parts.a2, float_bits.parts.a3,
		float_bits.parts.a4, float_bits.parts.a5, float_bits.parts.a6,
		float_bits.parts.a7, float_bits.parts.a8, float_bits.parts.a9,
		float_bits.parts.a10, float_bits.parts.a11, float_bits.parts.a12,
		float_bits.parts.a13, float_bits.parts.a14, float_bits.parts.a15,
		float_bits.parts.a16, float_bits.parts.a17, float_bits.parts.a18,
		float_bits.parts.a19, float_bits.parts.a20, float_bits.parts.a21,
		float_bits.parts.a22, float_bits.parts.a23, float_bits.parts.a24,
		float_bits.parts.a25, float_bits.parts.a26, float_bits.parts.a27,
		float_bits.parts.a28, float_bits.parts.a29, float_bits.parts.a30,
		float_bits.parts.a31, float_bits.parts.a32);
}