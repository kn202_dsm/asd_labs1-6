﻿#include <iostream>
#include <string>

struct date{
    unsigned char secunds : 6;
    unsigned char minutes : 6;
    unsigned char hours : 5;
    unsigned char nWeekDay : 3;
    unsigned char nMonthDay : 5;
    unsigned char nMonth : 4;
    unsigned long nYear : 11;

    date(unsigned char hours, unsigned char minutes, unsigned char secunds, 
        unsigned char nWeekDay, unsigned char nMonthDay, unsigned char nMonth, unsigned long nYear) {
        this->secunds = secunds;
        this->minutes = minutes;
        this->hours = hours;
        this->nWeekDay = nWeekDay;
        this->nMonthDay = nMonthDay;
        this->nMonth = nMonth;
        this->nYear = nYear;
    }
    void PrintDate() {
        std::string day[] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        std::cout << (int)hours << ':' << 
            (int)minutes << ':' << 
            (int)secunds << ' ' << 
            day[nWeekDay] << ' ' << 
            (int)nMonthDay << '.' << 
            (int)nMonth << '.' << 
            (long)nYear << std::endl;
    }

    date() {
        secunds = rand() % 60;
        minutes = rand() % 60;
        hours = rand() % 24;
        nWeekDay = rand() % 7;
        nMonthDay = rand() % 30;
        nMonth = rand() % 12;
        nYear = 1900 + (int)((float)rand() / (float)RAND_MAX * (2021 - 1900));
    }
};

void RandTmDate(tm &date) {
    date.tm_year = 1900 + (int)((float)rand() / (float)RAND_MAX * (2021 - 1900));
    date.tm_mon = rand() % 12;
    date.tm_mday = rand() % 30;
    date.tm_wday = rand() % 7;
    date.tm_hour = rand() % 24;
    date.tm_min = rand() % 60;
    date.tm_sec = rand() % 60;
}

void PrintTmDate(tm date) {
    std::string day[] = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
    std::cout << (int)date.tm_hour << ':' <<
        (int)date.tm_min << ':' <<
        (int)date.tm_sec << ' ' <<
        day[date.tm_wday] << ' ' <<
        (int)date.tm_mday << '.' <<
        (int)date.tm_mon << '.' <<
        (long)date.tm_year << std::endl;
}

int main()
{
    srand(time(NULL));
    date my_date;
    tm tm_date;
    printf("my_date:  ");
    my_date.PrintDate();

    printf("tm_date:  ");
    RandTmDate(tm_date);
    PrintTmDate(tm_date);
    
    std::cout << "\nSize of my date structure: " << sizeof(my_date) << "\nSize of standart date structure: " << sizeof(tm_date) << std::endl;
}
