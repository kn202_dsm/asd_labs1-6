﻿#include <iostream>

void ShowBytes(short value)
{
	union {
		short num;
		struct {
			;
			unsigned short b : 8;
			unsigned short a : 8;
		}parts;
	} m_short;
	m_short.num = value;
	//%p -> pointer (addres), %2.x (.2) -> 2 hex values with prefix '0', x -> hex //
	printf("first: %2.X(hex)\n", m_short.parts.a);
	printf("second: %2.X(hex)\n", m_short.parts.b);
}


int main()
{
	srand(time(NULL));
	union
	{
		short num;
		struct {
			unsigned short value : 15;
			unsigned short sign : 1;
		} bytes;
	} x;

	printf("Enter short value: ");
	scanf_s("%hd", &x.num);
	printf("sign:");
	printf(x.bytes.sign ? " - " : " + ");
	// printf( (x.bytes.sign & 1) ? " - " : " + "); // 
	printf("\nvalue: 0x%.hX(hex)\n", x.num);

	ShowBytes(x.num);
}
