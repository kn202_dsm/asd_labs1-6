﻿#include <iostream>
#include "chrono"

int main(void)
{
    long long a;
    int b, perevirka;
    int arr[65];
    printf("Input start value:  ");
    scanf_s("%d", &a);

    while (true)
    {
        printf("Input digit capacity (8 <= b <= 64)");
        scanf_s("%d", &b);
        if (b < 8 || b > 64)
            printf("Wrong value\nTry again\n\n");
        else
            break;
    }
    
    auto begin = std::chrono::steady_clock::now();
    getchar();
    for (int i = 0; i < b; i++)
    {
        if (a % 2 == 0)
        {
            a = a / 2;
            arr[i] = 0;
        }
        else
        {
            a = (a - 1) / 2;
            arr[i] = 1;
        }
    }
    printf("\n");
    for (int i = b - 1; i >= 0; i--)
    {
        printf("%d", arr[i]);
    }
    printf("\n");
    auto end = std::chrono::steady_clock::now();
    auto elapsed_nano = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    std::cout << "Function Execution Time: " << elapsed_nano.count() << " nano\n";
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
