﻿#include <iostream>

int main()
{
	srand(time(NULL));

	int menu;
	double num, factorial = 1;
	printf("Change function: \n 1.f(n) = n\n 2.f(n) = log(n)\n 3.f(n) = n * log(n)\n 4.f(n) = n^2\n 5.f(n) = 2^n\n 6.f(n) = n!\n");
	scanf_s("%d", &menu);
	switch (menu)
	{
	case 1: printf("\t\tf(n)=n\n");
		for (int i = 1; i <= 50; i++)
		{
			num = i;
			printf("f(%d)=%.1f\n", i, num);
		}
		break;
	case 2: printf("\t\tf(n)=log(n)\n");
		for (int i = 1; i <= 50; i++)
		{
			num = log(i);
			printf("f(%d)=%.2f\n", i, num);
		}
		break;
	case 3: printf("\t\tf(n)=n*log(n)\n");
		for (int i = 1; i <= 50; i++)
		{
			num = i * log(i);
			printf("f(%d)=%.1f\n", i, num);
		}
		break;
	case 4: printf("\t\tf(n)=n^2\n");
		for (int i = 1; i <= 50; i++)
		{
			num = pow(i, 2);
			printf("f(%d)=%.1f\n", i, num);
		}
		break;
	case 5:
		printf("\t\tf(n)=2^n\n");
		for (int i = 1; i <= 50; i++)
		{
			num = pow(2, i);
			printf("f(%d)=%.1f\n", i, num);
		}
		break;
	case 6: printf("\t\tf(n)=n!\n");
		for (int i = 1; i <= 50; i++)
		{
			factorial *= i;
			printf("f(%d)=%.1f\n", i, factorial);
		}
		break;
	}
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
