﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ASD_Lab5_1
{
    public static class Ex_c
    {
        static void SortInsertionLinkedList(LinkedList<int> list) // Сортування двозв'язного списку вставками
        {
            LinkedListNode<int> node = list.First;
            for (LinkedListNode<int> iNode = node.Next; iNode != null; iNode = iNode.Next)
            {
                int temp = iNode.Value;
                for (LinkedListNode<int> jNode = iNode.Previous; jNode != null && jNode.Value > temp; jNode = jNode.Previous)
                {
                    jNode.Next.Value = jNode.Value;
                    jNode.Value = temp;
                }
            }
        }

        public static void Ex_c_start()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            LinkedList<int> list2 = new LinkedList<int>();
            int size;
            Random rnd = new Random();
            Console.WriteLine("в) Сортування двозв'язного списку вставками");
            Console.WriteLine("Введіть розмір масиву: ");
            size = Convert.ToInt32(Console.ReadLine());

            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(200);
                list2.AddLast(random);
            }

            foreach (var item in list2)
            {
                Console.Write("\t" + item);
            }

            SortInsertionLinkedList(list2);
            Console.Write("\n\n\n\n\n");
            foreach (var item in list2)
            {
                Console.Write("\t" + item);
            }
            Console.WriteLine("\n\n");
            stopwatch.Stop();
            Console.WriteLine("Час сортування: " + stopwatch.Elapsed);
            Console.WriteLine("\n\n");
        }

    }
}
