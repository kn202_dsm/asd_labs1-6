﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASD_Lab5_1
{
    public static class Ex_b
    {
        public static void Ex_b_start()
        {
            Console.OutputEncoding = System.Text.Encoding.Default;
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            Console.WriteLine("б) Сортування массиву вставками\n");
            stopwatch.Start();
            Random rnd = new Random();
            int n;
            Console.WriteLine("Введіть розмір масиву: ");
            n = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[n];
            int counter = 0;
            for (int i = 0; i < n; i++)
            {
                array[i] = rnd.Next(-10, 10);
            }
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{array[i]}\t");
            }
            Console.WriteLine("\t\t\t\t\t\tВідсортований масив");
            for (int i = 0; i < n; i++)
            {
                for (int j = i; j > 0 && array[j - 1] > array[j]; j--)
                {
                    counter++;
                    int tmp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = tmp;
                }
            }
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{array[i]}\t");
            }
            stopwatch.Stop();
            Console.WriteLine("\nЧас виконання: " + stopwatch.Elapsed);
        }
    }
}
