﻿using ASD_Lab5_1;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASD_Lab5
{
    class Program
    {
        static int CheckValue()
        {
            int n;
            while (!int.TryParse(Console.ReadLine(), out n))
                Console.WriteLine("Неверное значение. Попробуйте ещё раз");
            return n;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Выберите способ сортировки: \n1)Сортировка выбором двозвязного списка\n2)Сортировка вставками\n3)Сортировка двозвязного списка вставками");
            switch (CheckValue())
            {
                case 1:
                    for (int i = 0; i < 5; i++)
                    {
                        Ex_a.Ex_a_start();
                    }
                    break;
                case 2:
                    for (int i = 0; i < 5; i++)
                    {
                        Ex_b.Ex_b_start();
                    }
                    break;
                case 3:
                    for (int i = 0; i < 5; i++)
                    {
                        Ex_c.Ex_c_start();
                    }
                    break;
            }
        }
    }
}
