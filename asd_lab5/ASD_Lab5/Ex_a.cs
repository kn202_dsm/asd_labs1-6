﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASD_Lab5_1
{
    public static class Ex_a
    {
        static void SortSelectionLinkedList(LinkedList<int> list) // Сортування вибором двозв'язного списку
        {
            for (LinkedListNode<int> iNode = list.First; iNode != null; iNode = iNode.Next)
            {
                LinkedListNode<int> minNode = iNode;
                for (LinkedListNode<int> jNode = iNode.Next; jNode != null; jNode = jNode.Next)
                    if (jNode.Value < minNode.Value)
                        minNode = jNode;

                // Перестановка значень
                int temp = iNode.Value;
                iNode.Value = minNode.Value;
                minNode.Value = temp;
            }
        }

        public static void Ex_a_start()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            LinkedList<int> list1 = new LinkedList<int>();
            Random rnd = new Random();


            int size;
            Console.WriteLine("a) Сортування вибором двозв'язного списку");
            Console.WriteLine("Введіть розмір масиву: ");
            size = Convert.ToInt32(Console.ReadLine());

            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(200);
                list1.AddLast(random);
            }

            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }

            Console.WriteLine("\n\n\n\n\n");

            SortSelectionLinkedList(list1);

            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }
            Console.WriteLine("\n\n");
            stopwatch.Stop();
            Console.WriteLine("ГХС: " + stopwatch.Elapsed);
            Console.WriteLine("\n\n");
        }
    }
}
