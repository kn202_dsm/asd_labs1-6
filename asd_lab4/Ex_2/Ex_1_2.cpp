﻿#include <stdio.h>
#include <stdlib.h>
#include "clist.h"

void printNode(elemtype* value) {
    printf("%d\n", *((int*)value));
}
// Додавання нового вузла за індексом в список
int insertNode(cList* list, size_t index, elemtype* data)
{
    if (index == 0) {
        pushFront(list, data);
        return(0);
    }
    cNode* node = getNode(list, index - 1);
    cNode* insertNode = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return(-5);
    }
    if (!insertNode) {
        return(-6);
    }
    insertNode->value = data;
    insertNode->prev = node;
    insertNode->next = node->next;
    if (node->next) {
        node->next->prev = insertNode;
    }
    node->next = insertNode;
    if (!node->prev) {
        list->head = node;
    }
    if (!node->next) {
        list->tail = node;
    }
    list->size++;
    return(0);
}
// Видалення вузла за індексом з списку
int deleteNode(cList* list, size_t index) {
    cNode* node = getNode(list, index);
    if (!node) {
        return(-7);
    }
    if (node->prev) {
        node->prev->next = node->next;
    }
    if (node->next) {
        node->next->prev = node->prev;
    }
    if (!node->prev) {
        list->head = node->next;
    }
    if (!node->next) {
        list->tail = node->prev;
    }
    free(node);
    list->size--;
    return(0);
}

int main() {
    elemtype a = 10;
    elemtype b = 11;
    elemtype c = 12;
    elemtype tmp;
    cList* mylist = createList();

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    popBack(mylist, &tmp);

    pushFront(mylist, &b);
    printList(mylist, printNode);
    printf("\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    pushBack(mylist, &c);
    printList(mylist, printNode);
    printf("\n");

    popFront(mylist, &tmp);
    printList(mylist, printNode);
    printf("\n");

    pushBack(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    printNode(getNode(mylist, 3)->value);
    printf("\n");

    printList(mylist, printNode);
    printf("\n");

    deleteList(mylist);

    system("pause");
    return 0;
}
